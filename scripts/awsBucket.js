const AWS = require("aws-sdk");

AWS.config.update({
  accessKeyId: process.env.AWS_IAM_ACCESS_KEY,
  secretAccessKey: process.env.AWS_IAM_SECRET_KEY,
});

var s3 = new AWS.S3();
