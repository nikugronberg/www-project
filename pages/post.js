import {withRouter} from "next/router"
import Layout from "../comps/layout"
import Header from "../comps/header"

const Content = withRouter((props) => (
  <div>
    <h1>{props.router.query.title}</h1>
    <p>This is the blog post content</p>
  </div>
));

const Page = (props) => (
  <div>
    <Header />
    <Layout>
      <Content />
    </Layout>
  </div>
);

export default Page
