//import next stuff
import Link from "next/link";


//import components
import Layout from "../comps/layout";
import Header from "../comps/header"
import HugePictureCarousel from "../comps/hugePictureCarousel";

const divStyle = {
  width: "100%",
}


const Index = () => (
  <div style={divStyle}>
    <Header />
    <HugePictureCarousel src={["/static/img1.png", "/static/img3.jpg" , "/static/img2.jpg", "/static/img4.jpg"]} />
    <Layout>
    </Layout>
  </div>
);

export default Index
