import Layout from "../comps/layout"
import Link from "next/link"
import Header from "../comps/header"

const PostLink = (props) => (
  <li>
    <Link as={`/p/${props.id}`} href={`/post?title=${props.title}`}>
      <a>{props.title}</a>
    </Link>
  </li>
);

const About = () => (
  <div>
    <Header />
    <Layout>
      <PostLink title="Hello" id="Hello"/>
      <PostLink title="World" id="World"/>
      <PostLink title="One more link" id="One more link"/>
    </Layout>
  </div>
);



export default About
