// ./pages/_document.js
import Document, { Head, Main, NextScript } from 'next/document'

const bodyStyle = {
  margin: 0,
  padding: 0,
  border: 0,
}
export default class MyDocument extends Document {
  render() {
    return (
      <html>
        <Head>
          <link rel="stylesheet" href="/_next/static/style.css" />
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet" />
        </Head>
        <body style={bodyStyle}>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
