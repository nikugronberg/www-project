import {CSSTransition} from "react-transition-group"
import "../css/fadeAnimation.css"

const duration = 2000;
const animation = "fade";


class HugePicture extends React.Component {
  constructor(props) {
    super(props);
    this.state = {width: 0, height: 0, targetWidth: props.width, targetHeight: props.height, ratio: 0, in: props.in, src: props.src};
    this.imgRef = React.createRef();

    this.calculateRatio = this.calculateRatio.bind(this);

  }


  render() {

    //calculating image size while maintaining aspect ratio

    if(this.imgRef.current !== null) {

      if(this.imgRef.current.height != this.state.targetHeight) {
        this.state.height = this.state.targetHeight;
        this.state.width = this.state.height * this.state.ratio;
        console.log("current: " + this.state.height + " target: " + this.state.targetHeight + " ratio: " + this.state.ratio);
      }
      if(this.state.width < this.state.targetWidth) {
        this.state.width = this.state.targetWidth;
        this.state.height = this.state.width / this.state.ratio;
      }
    }
    //console.log(this.state.in + " " + this.props.src);

    return (
      <CSSTransition
        in={this.state.in}
        timeout={duration}
        classNames={animation}
        onEnter={this.calculateRatio}
        unmountOnExit
        >

        <img
          src={this.state.src}
          height={this.state.height}
          width={this.state.width}
          onLoad={this.calculateRatio}
          ref={this.imgRef}
          />
      </CSSTransition>
    );
  }

  componentDidMount() {
    this.calculateRatio;
  }

  componentWillReceiveProps(props) {
    this.setState({in: props.in});
    this.setState({targetWidth: props.width, targetHeight: props.height})
  }

  //wait for the image to load somehow somewhere
  calculateRatio() {
    this.state.ratio = this.imgRef.current.naturalWidth / this.imgRef.current.naturalHeight;
    this.forceUpdate();
  }


}

export default HugePicture
