
const buttonRightStyle = {
  position: "absolute",
  zIndex: 90,
  right: "2em",
  top: "50%",
  background: "rgba(0,0,0,0)",
  border: "none",
  outline: 0,

}

const iconStyle = {
  fontSize: "6em",
}

const ButtonRight = (props) => (
  <button
    type="button"
    style={buttonRightStyle}
    onClick={props.buttonClick}
    >
      <i className="material-icons" style={iconStyle}>arrow_forward_ios</i>

  </button>
);

export default ButtonRight
