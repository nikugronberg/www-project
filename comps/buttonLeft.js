
const buttonLeftStyle = {
  position: "absolute",
  zIndex: 90,
  left: "2em",
  top: "50%",
  background: "rgba(0,0,0,0)",
  border: "none",
  outline: 0,

}

const iconStyle = {
  fontSize: "6em",
}

const ButtonLeft = (props) => (
  <button
    type="button"
    style={buttonLeftStyle}
    onClick={props.buttonClick}
    >
      <i className="material-icons" style={iconStyle}>arrow_back_ios</i>
  </button>
);

export default ButtonLeft
