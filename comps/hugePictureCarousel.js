import HugePicture from "./hugePicture"
import ButtonRight from "./buttonRight"
import ButtonLeft from "./buttonLeft"
import RadioButton from "./radioButton"

const resizeDelay = 100;

const carouselStyle = {
  position: "absolute",
  width: "100%",
  height: "100%",
  left: 0,
  right: 0,
  margin: "none",
  border: "none",
  overflow: "hidden",
}

const liStyle = {
  listStyleType: "none",
  margin: 0,
  padding: 0,
}

//doesnt stay in the middle if window is very small
const radioButtonBoxStyle = {
  position: "absolute",
  bottom: "6em",
  left: 0,
  right: 0,
  width: "20em",
  margin: "auto",
  zIndex: 90,
}




class HugePictureCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: props.src,
      width: 0,
      height: 0,
      resizeTimer: null,
      slideIndex: 0,
      slideCount: 0,
    };
    this.elementRef = React.createRef();

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

  }

  //indexes are used as keys. This should not be a problem since nothing is changed
  render() {
    const pictures = this.state.items.map((item, i) => (
          <li key={i} style={liStyle}>
            <HugePicture
              src={item}
              height={this.state.height}
              width={this.state.width}
              in={i == this.state.slideIndex}
            />
          </li>
        ));

    const radioButtons = this.state.items.map((item, i) => (
      <RadioButton
        key={i}
        in={i == this.state.slideIndex}
        changeHandler={this.radioButtonChangeHandler.bind(this, i)}
        />
    ));

    return (
      <div style={carouselStyle} >
        <ButtonRight buttonClick={this.slideRight.bind(this)}/>
        <ButtonLeft buttonClick={this.slideLeft.bind(this)}/>

        <div ref={this.elementRef}>
          {pictures}
        </div>

        <div style={radioButtonBoxStyle}>
          {radioButtons}
        </div>

      </div>
    );
  }

  componentDidMount() {
    this.updateWindowDimensions();
    this.state.slideCount = this.elementRef.current.childNodes.length;
    window.addEventListener("resize", this.updateWindowDimensions);

  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    //update every 0.1s
    this.state.resizeTimer = null;
    this.state.resizeTimer = setTimeout(() => {
      this.setState({width: window.innerWidth, height: window.innerHeight/* - this.elementRef.current.offsetTop */ });

    }, resizeDelay);
  }

  slideRight() {
    const index = this.state.slideIndex;
    const count = this.state.slideCount;
    if(index < count - 1) {
      this.setState({slideIndex: index + 1});
    } else {
      this.setState({slideIndex: 0});
    }
  }

  slideLeft() {
    const index = this.state.slideIndex;
    const count = this.state.slideCount;
    if(index > 0) {
      this.setState({slideIndex: index - 1});
    } else {
      this.setState({slideIndex: count - 1});
    }
  }

  radioButtonChangeHandler(index) {
    this.setState({slideIndex: index});
  }


}

export default HugePictureCarousel
