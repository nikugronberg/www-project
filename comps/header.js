//import things
import Link from "next/link"
import Logo from "../static/wanderersCraftSymbol.svg"

const navbarHeight = "4em";

const linkStyle = {
  display: "inline-block",
  fontSize: "1.2em",
  fontFamily: "Helvetica, Verdana, Arial",
  textDecoration: "none",
  fontWeight: "bold",
  padding: "1em",
  color: "white",
  top: "50%",

}

const logoStyle = {
  height: "50%",
  overflow: "hidden",
  filter: "brightness(0) invert(1)",

}



const navbarStyle = {
  position: "fixed",
  top: 0,
  left: 0,
  right: 0,
  height: navbarHeight,
  zIndex: 100,
  margin: 0,
  border: "none",
  width: "100%",
  backgroundColor: "#363636",
}

const fillerStyle = {
  position: "relative",
  top: 0,
  left: 0,
  right: 0,
  height: navbarHeight,
  zIndex: -1,
  width: "100%",
  padding: 0,
  margin: 0,
  border: 0,
}

const Header = (props) => (
  <div>
    <div style={fillerStyle}>
    </div>
    <div style={navbarStyle}>
      <Link href="/index">
        <a style={linkStyle}><img src={Logo} style={logoStyle} /></a>
      </Link>
      <Link href="/about">
        <a style={linkStyle}>About</a>
      </Link>
    </div>
  </div>
);



export default Header
