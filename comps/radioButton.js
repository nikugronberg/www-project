
const radioButtonStyle = {
  display: "inline",
  position: "static",
  margin: "0.1em",
  background: "rgba(0,0,0,0)",
  border: "none",
  outline: 0,
}

const iconStyle = {
  fontSize: "2em",
}

const RadioButton = (props) => (
    <button
      type="button"
      style={radioButtonStyle}
      onClick={props.changeHandler}>
        <i className="material-icons" style={iconStyle}>{props.in ? "radio_button_checked" : "radio_button_unchecked"}</i>
    </button>
);

export default RadioButton;
